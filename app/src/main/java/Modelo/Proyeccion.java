package Modelo;

import android.database.Cursor;

import com.example.examencorte29_3.Productos;

import java.util.ArrayList;

public interface Proyeccion {
    public Productos getProducto(int Codigo);
    public ArrayList<Productos> allProducto();
    public Productos readProductos(Cursor cursor);
}
