package Modelo;

import com.example.examencorte29_3.Productos;

public interface Persistencia {
    public void openDataBase();
    public void closeDataBase();
    public long insertProductos(Productos productos);
    public long updateProductos(Productos productos);
    public void deleteProductos(int codigo);
}
