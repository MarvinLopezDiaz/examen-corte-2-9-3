package Modelo;

import android.provider.AlarmClock;

public class DefineTabla {
    public DefineTabla(){}

    //Definicion de la tabla
    public static abstract class Sistema{
        public static final String TABLE_NAME = "Productos";
        public static final String COLUMN_NAME_CODIGO = "Codigo";
        public static final String COLUMN_NAME_MARCA = "marca";
        public static final String COLUMN_NAME_NOMBRE = "nombre";
        public static final String COLUMN_NAME_PRECIO = "precio";
        public static final String COLUMN_NAME_PRECEDENCIA = "precedencia";
    }

    //Objeto para los registros
    public static String[] REGISTRO = new String[]{
            Sistema.COLUMN_NAME_CODIGO,
            Sistema.COLUMN_NAME_MARCA,
            Sistema.COLUMN_NAME_NOMBRE,
            Sistema.COLUMN_NAME_PRECIO,
            Sistema.COLUMN_NAME_PRECEDENCIA

    };
}
