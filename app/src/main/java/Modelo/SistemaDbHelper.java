package Modelo;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class SistemaDbHelper extends SQLiteOpenHelper{
    private static final String TEXT_TYPE = " TEXT";
    private static final String INTEGER_TYPE = " INTEGER";
    private static final String COMA_SEP = " ,";

    private static final String SQL_CREATE_PRODUCTOS = "CREATE TABLE " +
            DefineTabla.Sistema.TABLE_NAME + " (" +
            DefineTabla.Sistema.COLUMN_NAME_CODIGO + " INTEGER PRIMARY KEY, " +
            DefineTabla.Sistema.COLUMN_NAME_MARCA + TEXT_TYPE + COMA_SEP +
            DefineTabla.Sistema.COLUMN_NAME_NOMBRE + TEXT_TYPE + COMA_SEP +
            DefineTabla.Sistema.COLUMN_NAME_PRECIO + TEXT_TYPE + COMA_SEP +
            DefineTabla.Sistema.COLUMN_NAME_PRECEDENCIA + TEXT_TYPE + ")";

    private static final String SQL_DELETE_PRODUCTOS = "DROP TABLE IF EXISTS " +
            DefineTabla.Sistema.TABLE_NAME;

    //Cambiar el nombre cuando actualice la estructura de la tabla
    private static final String DATABASE_NAME = "sistema.db";
    private static final   int DATABASE_VERCION = 1;

    public SistemaDbHelper (Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERCION);
    }

    //FUNCION PARA CRAR LA BASE DE DATOS
    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(SQL_CREATE_PRODUCTOS);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int il) {
        sqLiteDatabase.execSQL(SQL_DELETE_PRODUCTOS);
        onCreate(sqLiteDatabase);
    }

}