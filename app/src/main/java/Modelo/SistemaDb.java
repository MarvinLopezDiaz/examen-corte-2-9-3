package Modelo;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.example.examencorte29_3.Productos;

import java.util.ArrayList;

public class SistemaDb implements Persistencia, Proyeccion{
    private Context context;
    private SistemaDbHelper helper;
    private SQLiteDatabase db;

    public SistemaDb(Context context, SistemaDbHelper helper){
        this.context = context;
        this.helper = helper;
    }

    public SistemaDb(Context context){
        this.context = context;
        this.helper = new SistemaDbHelper(this.context);
    }


    @Override
    public void openDataBase() {
        db = helper.getWritableDatabase();
    }

    @Override
    public void closeDataBase() {
        helper.close();
    }

    @Override
    public long insertProductos(Productos productos) {
        return 0;
    }

    @Override
    public long updateProductos(Productos productos) {
        return 0;
    }

    @Override
    public void deleteProductos(int codigo) {

    }


    public long insertPrducto(Productos productos) {

        ContentValues values = new ContentValues();

        values.put(DefineTabla.Sistema.COLUMN_NAME_CODIGO, productos.getCodigo());
        values.put(DefineTabla.Sistema.COLUMN_NAME_NOMBRE, productos.getNombre());
        values.put(DefineTabla.Sistema.COLUMN_NAME_PRECIO, productos.getPrecio());
        values.put(DefineTabla.Sistema.COLUMN_NAME_PRECEDENCIA, productos.getPrecedencia());

        this.openDataBase();
        long num = db.insert(DefineTabla.Sistema.TABLE_NAME, null, values);
        //this.closeDataBase();
        Log.d("agregar", "insertAlumno" + num);

        return num;
    }

    public long updatePrducto(Productos productos) {
        ContentValues values = new ContentValues();

        values.put(DefineTabla.Sistema.COLUMN_NAME_CODIGO, productos.getCodigo());
        values.put(DefineTabla.Sistema.COLUMN_NAME_NOMBRE, productos.getNombre());
        values.put(DefineTabla.Sistema.COLUMN_NAME_PRECIO, productos.getPrecio());
        values.put(DefineTabla.Sistema.COLUMN_NAME_PRECEDENCIA, productos.getPrecedencia());

        this.openDataBase();
        long num = db.update(
                DefineTabla.Sistema.TABLE_NAME,
                values,
                DefineTabla.Sistema.COLUMN_NAME_CODIGO + " = " + productos.getCodigo(),
                null);

        this.closeDataBase();

        return num;
    }

    public void deletePrducto(String codigo) {
        this.openDataBase();
        db.delete(
                DefineTabla.Sistema.TABLE_NAME,
                DefineTabla.Sistema.COLUMN_NAME_CODIGO + "=?",
                new String[] {String.valueOf(codigo)});

        this.closeDataBase();
    }

    public Productos getProductos(String codigo) {

        db = helper.getWritableDatabase();

        Cursor cursor = db.query(
                DefineTabla.Sistema.TABLE_NAME,
                DefineTabla.REGISTRO,
                DefineTabla.Sistema.COLUMN_NAME_CODIGO + " = ?",
                new String[] {codigo},
                null, null, null);

        cursor.moveToFirst();
        Productos productos = readProductos(cursor);

        return productos;
    }

    @Override
    public Productos getProducto(int Codigo) {
        return null;
    }

    @Override
    public ArrayList<Productos> allProducto() {
        db = helper.getWritableDatabase();

        Cursor cursor = db.query(
                DefineTabla.Sistema.TABLE_NAME,
                DefineTabla.REGISTRO,
                null,null,null, null, null);

        ArrayList<Productos> alumnos = new ArrayList<Productos>();
        cursor.moveToFirst();

        while(!cursor.isAfterLast()){
            Productos productos = readProductos(cursor);
            productos.add(productos);
            cursor.moveToNext();
        }

        cursor.close();
        return productos;
    }

    @Override
    public Productos readProductos(Cursor cursor) {
        Productos productos = new Productos();

        productos.setCodigo(cursor.getInt(0));
        productos.setNombre(cursor.getString(2));
        productos.setPrecio(cursor.getString(3));
        productos.setPrecedencia(cursor.getString(4));

        return productos;
    }
}
