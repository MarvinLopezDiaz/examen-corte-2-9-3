package com.example.examencorte29_3;

import java.io.Serializable;
import java.util.ArrayList;

public class Productos implements Serializable {
    private int codigo;
    private String nombre;
    private String precio;
    private String precedencia;



    public Productos(){
        this.codigo = 0;
        this.nombre = "";
        this.precio = "";
        this.precedencia = "";
    }

    public Productos (int codigo,String nombre, String precio, String precedencia){
        this.codigo = codigo;
        this.nombre = nombre;
        this.precio = precio;
        this.precedencia = precedencia;
    }

    public int getCodigo(){return codigo;}
    public String getNombre(){return nombre;}
    public String getPrecio(){return precio;}
    public String getPrecedencia(){return precedencia;}

    public void setCodigo(int codigo){this.codigo = codigo;}
    public void setNombre(String nombre){this.nombre = nombre;}
    public void setPrecio(String precio){this.precio = precio;}
    public void setPrecedencia(String precedencia){this.precedencia = precedencia;}
}
