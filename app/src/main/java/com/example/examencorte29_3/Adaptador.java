package com.example.examencorte29_3;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.icu.text.Transliterator;
import android.net.Uri;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.io.File;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.stream.Collectors;

import android.widget.*;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import java.util.Locale;

import org.jetbrains.annotations.NotNull;

public class Adaptador extends RecyclerView.Adapter<Adaptador.ViewHolder> implements View.OnClickListener{
    protected ArrayList<Productos> listProductos;
    private ArrayList<Productos> listaOri;
    private View.OnClickListener listener;
    private LayoutInflater inflater;
    private  Context context;
    private ArrayList<Productos> originalList;

    public Adaptador(ArrayList<Productos> listProductos, Context context){
        this.listProductos = listProductos;
        this.context = context;
        this.inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
    }

    @NonNull
    @Override
    public Adaptador.ViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType){
        View view = inflater.inflate(R.layout.activity_editar_productos, null);
        view.setOnClickListener(this);  //Escucha el evento click
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Adaptador.ViewHolder holder, int position) {
        Productos productos = listProductos.get(position);
        holder.txtCodigo.setText(productos.getCodigo());
        holder.txtNombre.setText(productos.getNombre());
        holder.txtPrecio.setText(productos.getPrecio());
        holder.raPrece.setText(productos.getPrecedencia());
        holder.raNoPrece.setText(productos.getPrecedencia());

    }

    @Override
    public int getItemCount() { return listProductos.size(); }

    public void setOnClickListener(View.OnClickListener listener){ this.listener = listener;}

    @Override
    public void onClick(View view) { if(listener != null) listener.onClick(view); }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private LayoutInflater inflater;
        private TextView txtCodigo, txtNombre, txtPrecio;
        private RadioButton raPrece, raNoPrece;

        public ViewHolder(@NonNull @NotNull View itemView){
            super(itemView);
            txtNombre = (TextView) itemView.findViewById(R.id.txtNombre);
            txtCodigo = (TextView) itemView.findViewById(R.id.txtCodigo);
            txtPrecio = (TextView) itemView.findViewById(R.id.txtPrecio);

            raPrece = (RadioButton) itemView.findViewById((R.id.raPrece));
            raNoPrece = (RadioButton) itemView.findViewById((R.id.raNoPrece));

        }
    }
    public Filter getFilter(){
        return new Filter(){
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults filterResults = new FilterResults();
                ArrayList<Productos> listaFiltrada = new ArrayList<>();

                if(constraint == null || constraint.length() == 0){
                    listaFiltrada.addAll(listProductos);
                    filterResults.values = listaFiltrada;
                    filterResults.count = listaFiltrada.size();

                }else {
                    String searchStr = constraint.toString().toLowerCase().trim();

                    for (Productos itemProductos : listProductos) {

                        if (itemProductos.getNombre().toLowerCase().contains(searchStr)) {
                            listaFiltrada.add(itemProductos);
                        }
                    }
                    filterResults.values = listaFiltrada;
                    filterResults.count = listaFiltrada.size();
                }
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                listProductos.clear();
                if (results.count > 0){
                    listProductos.addAll((ArrayList<Productos>) results.values);
                }
                notifyDataSetChanged();
            }
        };
    }
}
