package com.example.examencorte29_3;

import android.app.Application;
import android.util.Log;

import java.util.ArrayList;

import Modelo.SistemaDb;

public class Aplicacion extends Application {
    static ArrayList<Productos> productos;

    public ArrayList<Productos> getProductos(){ return productos; }

    static SistemaDb sistemaDb;

    @Override
    public void onCreate(){
        super.onCreate();
        sistemaDb = new SistemaDb(getApplicationContext());
        //alumnos = ItemAlumno.llenarAlumnos();
        productos = sistemaDb.allProducto();
        sistemaDb.openDataBase();

        Log.d("","OnCreate: Tamañi array list: " + this.productos.size());
    }
}
